use std::io::{self, Write};
use std::collections::VecDeque;

use crate::process::Process;
use crate::command::cmd_is_done;
use colored::Colorize;

mod command;
mod process;
mod head;

#[warn(non_snake_case)]
fn main() -> std::io::Result<()> {

    let stdin = io::stdin();
    let mut processes: VecDeque<Process> = VecDeque::with_capacity(32);

    // Marque de fabrique
    head::init_head();

    loop {

        let mut user_input = String::with_capacity(256);
        // ECRITURE sur le STDOUT
        let stdout = io::stdout();

        // Verifie les processus en background
        cmd_is_done(&mut processes)?;


        {
            print!("{}","~~~~".cyan());
            let mut handle = stdout.lock();     //STDOUT lock
            print!("{} {} {} ", "(>°^°)>".red().bold(), "~".cyan(), ":".red());
            handle.flush()?;                               //locked
        }

        stdin.read_line(&mut user_input)?;

        let user_input = user_input.trim_end();

        // Au cas où
        if user_input == "" { continue; }

        // Quitte la console
        if user_input == "exit" { break; }

        else if user_input == "filterEnv" {
            command::filter_env();
        }
            // Gestion du pipe
        else if user_input.contains(" | ") {
            let cmds: Vec<&str> = user_input.split(" | ").collect();
            command::call_pipe_cmd(cmds)?;
        }
            // Gestion commande background
        else if user_input.ends_with(&" &") {
            let num = processes.len() + 1;
            let child = command::call_bg_cmd(user_input.replace("&", " ").trim())?;

            println!("[{}]  {}", &num, &child.id());

            processes.push_back(Process::new(num,child.id(), child));
        }
        else {
            match command::call_cmd(user_input).is_ok() {
                true => (),
                false => println!("Commande Invalide !"),
            }
        }

    }

    Ok(())
}
