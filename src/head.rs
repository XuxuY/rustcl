use colored::*;

pub fn init_head() {
    println!("{}"," _____           _    _____ _".cyan().bold());
    println!("{}","|  __ \\         | |  / ____| |".cyan().bold());
    println!("{}","| |__) |_  _ ___| |_| |    | |".cyan().bold());
    println!("{}","|  _  / | | / __| __| |    | |".cyan().bold());
    println!("{}","| | \\ \\ |_| \\__ \\ |_| |____| |____".cyan().bold());
    println!("{}","|_|  \\_\\__,_|___/\\__|\\_____|______|".cyan().bold());
    println!("{}","\t\t\t--- Made by Xu ---".red());
}