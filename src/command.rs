use std::process::{Command, Stdio, Child, ExitStatus};
use std::io;
use std::env;
use std::collections::{VecDeque, HashMap};

use crate::process::Process;

pub fn call_cmd(cmd: &str) -> Result<ExitStatus, io::Error>{

    let cmd_elem: Vec<&str> = cmd.split(' ').collect();

    // commande classique qui renvoie statue
    let status = Command::new(cmd_elem[0])
        .args(&cmd_elem[1..cmd_elem.len()])
        .status()?;

    Ok(status)
}

fn first_pipe(cmd: &str) -> Result<Child, io::Error> {
    let cmd_elem: Vec<&str> = cmd.split(' ').collect();

    let child = Command::new(cmd_elem[0])
        .args(&cmd_elem[1..cmd_elem.len()])
        .stdout(Stdio::piped())
        .spawn()?;

    Ok(child)
}

pub fn call_pipe_cmd(cmds: Vec<&str>) -> Result<ExitStatus, io::Error> {

    //From : https://doc.rust-lang.org/stable/std/process/struct.Stdio.html#impl-From%3CChildStdout%3E

    // Si la commande était : ls | sleep 3 | rev

    // first_pipe récupère le Child de la première commande
    // (Dans l'exemple, on récupère le Child de la commande "ls")
    let mut child = first_pipe(cmds[0])?;
    let last_cmd_index = cmds.len() - 1;

    // Boucle sur les commandes restantes
    // (boucle sur "sleep 3" et "rev")
    for i in 1 .. cmds.len() {

        // Attend la fin du dernier processus lancé
        child.wait()?;

        let cmd_elem: Vec<&str> = cmds[i].split(' ').collect();

        // Crée la commande suivante
        // "sleep" si le commande précédente "ls" sinon "rev"
        let mut command: &mut Command = &mut Command::new(&cmd_elem[0]);
        command = command.args(&cmd_elem[1..cmd_elem.len()]);

        if i == last_cmd_index {
            // Récupère la stdout de la commande précédente
            let stdout = child.stdout.expect("Error converting stdio !");
            command = command.stdin(stdout);
        }
        else {
            // Récupère la stdout de la commande précédente et pipe la commande
            let stdout = child.stdout.expect("Error converting stdio !");
            command = command.stdin(stdout).stdout(Stdio::piped());
        }

        // Lance la commande
        child = command.spawn()?;

    }

    // Attend la fin du dernier processus lancé
    child.wait()
}


pub fn call_bg_cmd(cmd: &str) -> Result<Child, io::Error> {
    let cmd_elem: Vec<&str> = cmd.split(' ').collect();

    let child = Command::new(cmd_elem[0])
        .args(&cmd_elem[1..cmd_elem.len()])
        .spawn()?;

    Ok(child)
}

pub fn cmd_is_done(pro: &mut VecDeque<Process>) -> Result<(), io::Error>{

    let mut indexes: Vec<usize> = Vec::with_capacity(32);

    for i in 0..pro.len() {

        // Récupère les processus
        let p = &mut pro[i];
        let num = p.num();
        let id = p.id();

        // on vérifie si les processus sont terminés
        match p.child().try_wait() {
            Ok(Some(_)) => {
                println!("[{}] +{} Done", num, id);
                indexes.push(i);
            },
            Ok(None) => (),
            Err(_) => println!("error : try_wait !"),
        }
    }

    // Retire les processus finis
    update_bg_pro(pro, indexes);

    Ok(())
}

fn update_bg_pro(pro: &mut VecDeque<Process>, indexes: Vec<usize>) {
    for index in indexes {
        pro.remove(index);
    }
}

pub fn filter_env() {

//    https://doc.rust-lang.org/stable/std/process/struct.Command.html#method.envs

    let filtered_env : HashMap<String, String> =
        env::vars().filter(|&(ref k, _)|
            k == "TERM" || k == "TZ" || k == "LANG" || k == "PATH"
        ).collect();

    Command::new("printenv")
        .stdin(Stdio::null())
        .stdout(Stdio::inherit())
        .env_clear()
        .envs(&filtered_env)
        .status()
        .expect("printenv failed to start");
}