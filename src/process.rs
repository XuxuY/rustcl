use std::process::Child;

pub struct Process (
    usize,
    u32,
    Child,
);

impl Process {
    pub fn new(num: usize, id: u32, child: Child) -> Self {
        Self(num, id, child)
    }

    pub fn num(&self) -> usize {
        self.0
    }

    pub fn id(&self) -> u32 {
        self.1
    }

    pub fn child(&mut self) -> &mut Child {
        &mut self.2
    }
}

#[cfg(test)]
mod tests {
    use std::process::Command;
    use crate::process::Process;

    #[test]
    fn new_pro() {
        let mut child = Command::new("ls")
            .spawn()
            .expect("Commande introuvable ! ");

        let pro = Process::new(1, 2465, child);
        assert_eq!(pro.num(), 1);
        assert_eq!(pro.id(), 2465);
//        assert_eq!(pro.child(), &mut child);
    }
}
