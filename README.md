# RustCL

## Participant
- Yannick Xu

## Librairies utilisés
- [colored v1.9](https://crates.io/crates/colored)

## Dépôt Git
- [gitlab - RustCL](https://gitlab.com/XuxuY/rustcl)

## Réponses Questions

> **Note :** RustCL supporte les commandes simples `ls -al`, les pipes `ls -al | grep file | rev` et
> commande a run en arrière-plan `sleep 20 &`. Mais, il n'est pas capable de gérer le pipe + background
> `ls -al | sleep 10 &`.


1.2.1. Une reference représente un borrow d'une valeur ou variable.
En utilisant les opérateurs `&` ou `&mut`.
Elle est partagé et mutable.

1.2.2. `struct`, `enum` et `const` permettent de déclarer ses propres types.

1.2.3. Rust utilise `rustc` pour compile en assembleur natif.

1.2.4. Avec un processeur 8 bits, la valeur maximale adressable est 255. (0x0 -> 0xF)

1.2.5. Un processus est une commande en cours d'exécution dont on alloue un espace mémoire.
Un processus a un :
- PID (Procesus Identifier),
- PPID (Parent Processus Identifier),
- Terminal d’attache pour les entrées/sorties
[source](http://www-inf.telecom-sudparis.eu/COURS/CSC3102/Supports/ci5-processus/ci-processus.pptx.pdf)

2.1.1. `cargo run` compile et execute le programe.
	   `cargo test` exécute les tests.
	    les binaires sont rangés dans `target/debug/`.

3.1.5. Notre programme attend la fin du processus enfant, et récupère l'état de sortie.

4.1.7.  Un tupe entre deux programme est lorsque un processus prends en entrée le résultat en sortie du dernier processus.
Exemple : `ls | rev`: on applique la commande `rev` sur le résultat en sortie de la commande `ls`.
(wikipédia)

5.1.10. Un procesus ID (PID) est une caractéristique statique d'un processus, il ne change pas.
Un identifiant unique utilisé par le kernel pour identifier un processus actif.
(wikipédia)